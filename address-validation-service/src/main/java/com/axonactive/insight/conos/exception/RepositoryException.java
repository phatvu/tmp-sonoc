package com.axonactive.insight.conos.exception;

/**
 * Created by vhphat on 4/25/2015.
 */
public class RepositoryException extends RuntimeException {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

}