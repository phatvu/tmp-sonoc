package com.axonactive.insight.conos.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * Created by vhphat on 4/25/2015.
 */
@Repository
public abstract class BaseJdbcTemplateRepository {

    protected JdbcTemplate jdbcTemplate;

    public BaseJdbcTemplateRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
