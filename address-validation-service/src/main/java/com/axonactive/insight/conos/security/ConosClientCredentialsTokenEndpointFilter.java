package com.axonactive.insight.conos.security;

import com.axonactive.insight.conos.repository.ada.JdbcTemplateAdaRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by nqbao on 05/05/2015.
 */
public class ConosClientCredentialsTokenEndpointFilter extends ClientCredentialsTokenEndpointFilter {

    @Resource
    JdbcTemplateAdaRepository adaRepository;

    public void setAdaRepository(JdbcTemplateAdaRepository adaRepository) {
        this.adaRepository = adaRepository;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException, ServletException {
        adaRepository.insertTransaction(true);
        super.successfulAuthentication(request, response, chain, authResult);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        adaRepository.insertTransaction(false);
        super.unsuccessfulAuthentication(request, response, failed);
    }

}
