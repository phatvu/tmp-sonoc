package com.axonactive.insight.conos.repository.oau;

import com.axonactive.insight.conos.dto.OauClient;
import com.axonactive.insight.conos.repository.BaseJdbcTemplateRepository;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by natuan on 04/05/2015.
 */
public class JdbcTemplateOauRepository extends BaseJdbcTemplateRepository implements OauRepository {

    public JdbcTemplateOauRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public OauClient getOauClientByClientId(String clientId) {
        String sql = "SELECT * FROM oauth_client_details WHERE client_id = ?";
        try {
            return jdbcTemplate.queryForObject(sql, new Object[]{clientId}, new RowMapper<OauClient>() {
                @Override
                public OauClient mapRow(ResultSet resultSet, int i) throws SQLException {
                    OauClient oauClient = new OauClient();
                    oauClient.setClientId(resultSet.getString(1));
                    oauClient.setResourceIds(resultSet.getString(2));
                    oauClient.setClientSecret(resultSet.getString(3));
                    oauClient.setScope(resultSet.getString(4));
                    oauClient.setAuthorizedGrantTypes(resultSet.getString(5));
                    oauClient.setWebServerRedirectUri(resultSet.getString(6));
                    oauClient.setAuthorities(resultSet.getString(7));
                    oauClient.setAccessTokenValidity(resultSet.getInt(8));
                    oauClient.setRefreshTokenValidity(resultSet.getInt(9));
                    oauClient.setAdditionalInformation(resultSet.getString(10));
                    oauClient.setAutoapprove(resultSet.getString(11));
                    return oauClient;
                }
            });
        } catch (IncorrectResultSizeDataAccessException e) {
            return null;
        }
    }
}
