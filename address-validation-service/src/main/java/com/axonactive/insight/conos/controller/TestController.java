package com.axonactive.insight.conos.controller;

/**
 * Created by vhphat on 4/25/2015.
 */

import com.axonactive.insight.conos.repository.ada.AdaRepository;
import com.axonactive.insight.conos.repository.oau.JdbcTemplateOauRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

// will be deleted.
@RestController
public class TestController extends BaseController{

    @Autowired
    JdbcTemplateOauRepository jdbcTemplateOauRepository;

    @Resource
    AdaRepository adaRepository;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public Object sayHello() {
        Map<String, String> result = new HashMap<String, String>(){{
            put("name", "Sunwheel");
        }};
        return resultMap(true, result );
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Object test() {
        adaRepository.insertTransaction(true);
        Map<String, String> result = new HashMap<String, String>(){{
            put("name", "Sunwheel");
        }};
        return resultMap(true, result );
    }
}
