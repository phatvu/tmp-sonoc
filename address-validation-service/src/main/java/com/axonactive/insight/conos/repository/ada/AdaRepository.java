package com.axonactive.insight.conos.repository.ada;

import com.axonactive.insight.conos.dto.AdaTransaction;
import com.axonactive.insight.conos.dto.AdaUser;

/**
 * Created by vhphat on 4/25/2015.
 */
public interface AdaRepository {
    Boolean insertTransaction(boolean isTokenRequestSuccessful);
}
