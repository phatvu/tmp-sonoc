package com.axonactive.insight.conos.http;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpStatus;
import org.w3c.dom.Document;

/**
 * Created by vhphat on 4/25/2015.
 */
public class HttpResult {
    private int statusCode;

    private JsonNode rootJsonNode;

    private Object jsonObject;

    private Document rootDomNode;

    public HttpResult(int statusCode) {
        this.statusCode = statusCode;
    }

    public Object getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(Object jsonObject) {
        this.jsonObject = jsonObject;
    }

    public boolean isSuccessful() {
        return statusCode == HttpStatus.SC_OK;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public JsonNode getRootJsonNode() {
        return rootJsonNode;
    }

    public void setRootJsonNode(JsonNode rootJsonNode) {
        this.rootJsonNode = rootJsonNode;
    }

    public Document getRootDomNode() {
        return rootDomNode;
    }

    public void setRootDomNode(Document rootDomNode) {
        this.rootDomNode = rootDomNode;
    }
}
