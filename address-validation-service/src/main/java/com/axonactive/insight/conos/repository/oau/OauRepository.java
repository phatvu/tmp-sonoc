package com.axonactive.insight.conos.repository.oau;

import com.axonactive.insight.conos.dto.OauClient;

/**
 * Created by natuan on 04/05/2015.
 */
public interface OauRepository {
    OauClient getOauClientByClientId(String clientId);
}
