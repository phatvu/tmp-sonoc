package com.axonactive.insight.conos.dto;

/**
 * Created by ttnhan on 4/27/2015.
 */
public class AdaTransaction {
    private long id;
    private long userId;
    private long typeId;
    private String group;
    private double price;
    private String currencyCode;
    private long unit;
    private long quantity;
    private String id1;
    private String id2;
    private String information;
    private String clearingDate;
    private String modifyDate;
    private String modifyUser;
    private String createDate;
    private String createUser;

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public long getUnit() {
        return unit;
    }

    public void setUnit(long unit) {
        this.unit = unit;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getId1() {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getId2() {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getClearingDate() {
        return clearingDate;
    }

    public void setClearingDate(String clearingDate) {
        this.clearingDate = clearingDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public String getModifyUser() {
        return modifyUser;
    }

    public String getCreateDate() {
        return createDate;
    }

    public String getCreateUser() {
        return createUser;
    }
}
