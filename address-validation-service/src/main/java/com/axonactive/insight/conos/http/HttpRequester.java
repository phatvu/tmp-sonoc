package com.axonactive.insight.conos.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Created by vhphat on 4/25/2015.
 */
public class HttpRequester {
    private static Logger logger = LoggerFactory.getLogger(HttpRequester.class);

    private String serviceUrl;
    private Map<String, String> serviceHeaders;

    private ObjectMapper mapper = new ObjectMapper();
    private CloseableHttpClient httpclient;

    public HttpRequester(String vibeServiceUrl, Map<String, String> vibeServiceHeaders){

        this.serviceUrl = vibeServiceUrl;
        this.serviceHeaders = vibeServiceHeaders;

        try {
            // TODO : Must fix deprecated
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
            httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } catch (NoSuchAlgorithmException e) {
            // TODO Exception handling
            e.printStackTrace();
        } catch (KeyStoreException e) {
            // TODO Exception handling
            e.printStackTrace();
        } catch (KeyManagementException e) {
            // TODO Exception handling
            e.printStackTrace();
        }
    }

    public HttpResult makeHttpGetRequest(String url) {

        logger.info("Sending GET to URL: {}", url);

        HttpGet get = new HttpGet(url);

        return executeRequest(get);
    }

    private HttpResult executeRequest(HttpUriRequest request) {
        return executeRequest(request, null);
    }

    private HttpResult executeRequest(HttpUriRequest request, Class mapperClass) {

        HttpResult httpResult = new HttpResult(HttpStatus.SC_NOT_FOUND);

        CloseableHttpResponse r = null;

        try {

            r = httpclient.execute(request);

            httpResult.setStatusCode(r.getStatusLine().getStatusCode());

            // Check that we get a 200 indicating success
            if (r.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                InputStream inputStream = r.getEntity().getContent();
                if (mapperClass != null) {
                    httpResult.setJsonObject(mapper.readValue(inputStream, mapperClass));
                } else {
                    httpResult.setRootJsonNode(mapper.readTree(inputStream));
                }
            } else {
                logger.error("Non 200 response received for {} {} ", request.getURI(), r);
            }

        } catch (IOException e) {
            logger.error("Exception executing http request", e);
        } finally {
            if (r != null) {
                try {
                    r.close();
                } catch (IOException e) {
                    logger.error("Exception closing response", e);
                }
            }
        }

        return httpResult;

    }
}
