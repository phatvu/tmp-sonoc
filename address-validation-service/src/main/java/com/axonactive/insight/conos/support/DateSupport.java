package com.axonactive.insight.conos.support;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;

/**
 * Created by vhphat on 4/25/2015.
 */
public class DateSupport {
    public static boolean timeInRange(DateTime dateTimeToCheck, int startHour, int startMinute, int endHour,
                                      int endMinute, DateTimeZone timeZone) {

        LocalTime startTime = new LocalTime(startHour, startMinute);
        LocalTime endTime = new LocalTime(endHour, endMinute);

        DateTime st = startTime.toDateTimeToday(DateTimeZone.UTC).withZoneRetainFields(timeZone);
        DateTime et = endTime.toDateTimeToday(DateTimeZone.UTC).withZoneRetainFields(timeZone);

        return !(dateTimeToCheck.isBefore(st) || dateTimeToCheck.isAfter(et));

    }
}
