package com.axonactive.insight.conos.repository.ada;

import com.axonactive.insight.conos.dto.AdaTransaction;
import com.axonactive.insight.conos.dto.AdaUser;
import com.axonactive.insight.conos.repository.BaseJdbcTemplateRepository;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by vhphat on 4/25/2015.
 */
public class JdbcTemplateAdaRepository extends BaseJdbcTemplateRepository implements AdaRepository{

    public JdbcTemplateAdaRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    public Boolean insertTransaction(final boolean isTokenRequestSuccessful) {
        String sql = "insert into ada_tra (tra_usr_id, tra_trt_id, tra_ctrgrp, tra_info) " +
                "values (1, 30501, '1', ?);";
        Boolean insertSuccessfully = jdbcTemplate.execute(sql, new PreparedStatementCallback<Boolean>() {
            @Override
            public Boolean doInPreparedStatement(PreparedStatement preparedStatement) throws SQLException, DataAccessException {
                if (isTokenRequestSuccessful) {
                    preparedStatement.setString(1, "Token request successful");
                } else {
                    preparedStatement.setString(1, "Token request failed");
                }
                return preparedStatement.execute();
            }
        });
        return insertSuccessfully;
    }
}
