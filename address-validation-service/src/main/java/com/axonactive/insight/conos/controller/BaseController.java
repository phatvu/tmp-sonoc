package com.axonactive.insight.conos.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vhphat on 4/25/2015.
 */
public class BaseController {
    private static Logger logger = LoggerFactory.getLogger(BaseController.class);

    protected Map<String, Object> resultMap(final Boolean success, final Object result) {
        return new HashMap<String, Object>(){{
            put("success", success.toString());
            if(result != null) {
                put("result", result);
            }
        }};
    }

    @ExceptionHandler(Exception.class)
    public Object handleException(Exception exception) {
        logger.error("Exception processing request", exception);
        Map<String, Object> result = new HashMap<>();
        result.put("error", exception.toString());
        return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
    }
}
