package com.axonactive.insight.conos.security;

import com.axonactive.insight.conos.repository.ada.JdbcTemplateAdaRepository;
import org.apache.http.entity.ContentType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by nqbao on 05/05/2015.
 */
public class ConosAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Resource
    JdbcTemplateAdaRepository adaRepository;

    public void setAdaRepository(JdbcTemplateAdaRepository adaRepository) {
        this.adaRepository = adaRepository;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {
        adaRepository.insertTransaction(false);
        response.getWriter().append("{\"error\": \"Unauthorized: Authentication token was either missing or invalid.\"}");
        response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
}