package com.axonactive.insight.conos.security;

import com.axonactive.insight.conos.dto.OauClient;
import com.axonactive.insight.conos.repository.oau.OauRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nqbao
 */
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

	@Resource
	OauRepository oauRepository;

	public void setOauRepository(OauRepository oauRepository) {
		this.oauRepository = oauRepository;
	}

	@Override
	public ClientDetails loadClientByClientId(String clientId)
			throws OAuth2Exception {

		OauClient oauClient = oauRepository.getOauClientByClientId(clientId);

		if (oauClient != null) {
			
			List<String> authorizedGrantTypes=new ArrayList<>();
			authorizedGrantTypes.add("client_credentials");

			List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENT"));

			BaseClientDetails clientDetails = new BaseClientDetails();
			clientDetails.setClientId(oauClient.getClientId());
			clientDetails.setClientSecret(oauClient.getClientSecret());
			clientDetails.setAuthorizedGrantTypes(authorizedGrantTypes);
			clientDetails.setAccessTokenValiditySeconds(oauClient.getAccessTokenValidity());
			clientDetails.setAuthorities(grantedAuthorities);

			return clientDetails;
		}
		else{
			throw new NoSuchClientException("No client with requested id: "
					+ clientId);
		}
	}

}
