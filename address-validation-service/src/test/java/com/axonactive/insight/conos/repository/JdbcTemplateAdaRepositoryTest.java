package com.axonactive.insight.conos.repository;

import com.axonactive.insight.conos.BaseTestCase;
import com.axonactive.insight.conos.dto.AdaTransaction;
import com.axonactive.insight.conos.dto.AdaUser;
import com.axonactive.insight.conos.repository.ada.JdbcTemplateAdaRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.Mockito.when;

/**
 * Created by natuan on 29/04/2015.
 */
public class JdbcTemplateAdaRepositoryTest extends BaseTestCase {

    @InjectMocks
    JdbcTemplateAdaRepository jdbcTemplateAdaRepository;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Before
    public void init() {
    }

    @Test
    public void testInsertAdaTransactionSuccessfully() {
        AdaTransaction adaTransaction = new AdaTransaction();
        when(jdbcTemplate.execute(Mockito.anyString(), (PreparedStatementCallback<Boolean>) Mockito.anyObject())).thenReturn(true);
        Boolean expectedValue = jdbcTemplateAdaRepository.insertTransaction(true);
        assertTrue(expectedValue);
    }

    @Test
    public void testInsertAdaTransactionFailed() {
        AdaTransaction adaTransaction = new AdaTransaction();
        when(jdbcTemplate.execute(Mockito.anyString(), (PreparedStatementCallback<Boolean>) Mockito.anyObject())).thenReturn(false);
        Boolean expectedValue = jdbcTemplateAdaRepository.insertTransaction(true);
        assertFalse(expectedValue);
    }
}
