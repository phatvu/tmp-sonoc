package com.axonactive.insight.conos.repository;

import com.axonactive.insight.conos.BaseTestCase;
import com.axonactive.insight.conos.dto.AdaUser;
import com.axonactive.insight.conos.dto.OauClient;
import com.axonactive.insight.conos.repository.oau.JdbcTemplateOauRepository;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.Mockito.when;

/**
 * Created by natuan on 06/05/2015.
 */
public class JdbcTemplateOauRepositoryTest extends BaseTestCase {

    @InjectMocks
    JdbcTemplateOauRepository jdbcTemplateOauRepository;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Test
    public void testGetOauClientWithClientId() {
        OauClient oauClient = new OauClient();
        oauClient.setClientId("peax-client");
        when(jdbcTemplate.queryForObject(Mockito.anyString(), (Object[]) Mockito.anyObject(), (RowMapper<OauClient>) Mockito.anyObject())).thenReturn(oauClient);
        OauClient expectedResult = jdbcTemplateOauRepository.getOauClientByClientId("peax-client");
        assertNotNull(expectedResult);
    }

    @Test
    public void testGetOauClientWithInvalidClientId() {
        when(jdbcTemplate.queryForObject(Mockito.anyString(), (Object[]) Mockito.anyObject(), (RowMapper<OauClient>) Mockito.anyObject())).thenReturn(null);
        OauClient expectedResult = jdbcTemplateOauRepository.getOauClientByClientId("invalid-client-id");
        assertNull(expectedResult);
    }
}
