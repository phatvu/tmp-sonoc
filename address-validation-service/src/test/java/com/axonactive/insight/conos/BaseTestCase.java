package com.axonactive.insight.conos;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by natuan on 29/04/2015.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-spring-application.xml")
public class BaseTestCase extends TestCase {
    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
}
