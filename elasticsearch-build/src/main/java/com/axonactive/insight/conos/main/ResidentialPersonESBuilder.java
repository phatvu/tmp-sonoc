package com.axonactive.insight.conos.main;

import com.axonactive.insight.conos.dto.Name;
import com.axonactive.insight.conos.dto.ResidentialPerson;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by ttnhan on 4/29/2015.
 */
public class ResidentialPersonESBuilder {

    public static final long DEFAULT_BULK_SIZE = 1000;
    public static final String INDEX_NAME = "rpe";
    public static final String TYPE_NAME = "public";
    public static final String ELASTIC_SEARCH_HOST = "192.168.70.99";
    public static final Integer ELASTIC_SEARCH_API_PORT = 9300;
    public static final String ELASTIC_SEARCH_CLUSTER_NAME = "conos";

    ESSearcher nameExceptionES;
    private JdbcTemplate jdbcTemplate;
    private String indexName;
    private String typeName;
    private Client client;
    private long bulkSize;
    private boolean isUsingOriginalId;

    public ResidentialPersonESBuilder(String host, Integer port, String clusterName, String index, String type) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-application.xml");
        this.jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");

        Settings settings = ImmutableSettings.settingsBuilder().put("cluster.name", clusterName).build();
        this.client = new TransportClient(settings).addTransportAddress(new InetSocketTransportAddress(host, port));

        this.indexName = index;
        this.typeName = type;
        this.bulkSize = DEFAULT_BULK_SIZE;
        this.isUsingOriginalId = false;

        this.nameExceptionES = ESSearcher.getInstance();
        this.nameExceptionES.searchOnIndex("exception");
        this.nameExceptionES.searchOnType("name");
    }

    public void build(boolean isBuildAll, long bulkSize, boolean isUsingOriginalId) {
        if (bulkSize > 0) {
            this.bulkSize = bulkSize;
        }
        this.isUsingOriginalId = isUsingOriginalId;
        long startTime = System.currentTimeMillis();

        if (isBuildAll) {
            buildAll();
        } else {
            buildNewInsertedOrUpdated();
        }
        client.close();
        nameExceptionES.disconnect();

        long finishTime = System.currentTimeMillis();
        System.out.println("Finished in about " + (finishTime - startTime) / 1000 + " seconds.");
    }

    public void buildAll() {
        Long total = getTotalOfResidentialPersons();
        long numberOfBulks = (long) Math.ceil((total + 0.0) / bulkSize);
        long currentOffset = 0;

        for (long i = 0; i < numberOfBulks; i++) {
            List<ResidentialPerson> residentialPersons = getAllResidentialPersonByOffset(currentOffset);
            currentOffset += residentialPersons.size();
            int j;
            for (j = 0; j < residentialPersons.size(); j++) {
                try {
                    System.out.print(residentialPersons.get(j).getFirstName() + " - ");
                    enrichFirstNameSYN(residentialPersons.get(j));
                    System.out.println(residentialPersons.get(j).getFirstNameSYN());
                    buildResidentialPersonToES(residentialPersons.get(j));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.printf("%d item processed\n", i * bulkSize + j);
        }
    }

    private void buildNewInsertedOrUpdated() {
        // find new inserted or updated ids

        // fill bulk

        // build
    }

    private void buildResidentialPersonToES(ResidentialPerson residentialPerson) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(baos, residentialPerson);

        IndexRequest indexRequest = new IndexRequest(this.indexName, this.typeName);

        if (this.isUsingOriginalId) {
            indexRequest.id(Long.toString(residentialPerson.getPersonAddressNumber()));
        }
        indexRequest.source(baos.toString("UTF-8"));
        IndexResponse response = client.index(indexRequest).actionGet();
        if (response.getVersion() > 1) {
            System.out.println("Duplicated id: " + response.getId() + " " + response.getVersion());
        }
        baos.close();
    }

    private void enrichFirstNameSYN(ResidentialPerson residentialPerson) {
        Name nameWithReplacement;
        try {
            nameExceptionES.searchString("name", residentialPerson.getFirstName());
            nameWithReplacement = (Name) nameExceptionES.getUniqueResults(Name.class);
        } catch (NonUniqueResultException e) {
            System.out.println(e.getMessage());
            nameWithReplacement = (Name) e.getResultSet().get(0);
        }

        if (nameWithReplacement != null) {
            residentialPerson.setFirstNameSYN(nameWithReplacement.getReplacement());
        } else {
            String nameReplaceAsMode14 = residentialPerson.getFirstName();
            nameReplaceAsMode14 = nameReplaceAsMode14.replace("ph", "f");
            nameReplaceAsMode14 = nameReplaceAsMode14.replace("-", " ");
            residentialPerson.setFirstNameSYN(nameReplaceAsMode14);
        }
    }

    private Long getTotalOfResidentialPersons() {
        return jdbcTemplate.queryForObject("select count(*) from con_rpe", Long.class);
        //return 501L; // an example value
    }

    private ResidentialPerson getResidentialPersonById(String id) {
        String sql = "SELECT * FROM con_rpe WHERE rpe_id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[] {id}, ResidentialPerson.class);
    }

    private List<ResidentialPerson> getAllResidentialPersonByOffset(long offset) {
        String sql = "SELECT * FROM con_rpe order by rpe_id asc limit " + bulkSize + " offset " + offset;
        List<ResidentialPerson> residentialPersons = Collections.emptyList();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        residentialPersons = convertListOfMapToListOfResidentialPerson(rows);

        return residentialPersons;
    }

    private List<ResidentialPerson> convertListOfMapToListOfResidentialPerson(List<Map<String, Object>> resultSet) {
        List<ResidentialPerson> residentialPersons = new ArrayList<ResidentialPerson>();

        for (Map row: resultSet) {
            ResidentialPerson residentialPerson = convertMapToResidentialPerson(row);
            residentialPersons.add(residentialPerson);
        }

        return residentialPersons;
    }

    private static ResidentialPerson convertMapToResidentialPerson(Map resultRow) {
        ResidentialPerson residentialPerson = new ResidentialPerson();

        residentialPerson.setPersonAddressNumber((Long) (resultRow.get("rpe_id")));
        residentialPerson.setPersonNumber((Long) resultRow.get("rpe_pe_id"));
        residentialPerson.setHouseholdNumber((Long) resultRow.get("rpe_hh_nr"));
        residentialPerson.setStatusOfPerson((String) resultRow.get("rpe_cperstat"));
        residentialPerson.setPersonInactivationDate((Date) resultRow.get("rpe_inactivation_date"));
        residentialPerson.setLastConfirmationDate((Date) resultRow.get("rpe_confirmation_date"));
        residentialPerson.setStatusOfAddress((String) resultRow.get("rpe_cadrstat"));
        residentialPerson.setSourceOfAddress((String) resultRow.get("rpe_cadrsrc"));
        residentialPerson.setSourceOfRelocation((String) resultRow.get("rpe_crelsrc"));
        residentialPerson.setDeliveryProbability((String) resultRow.get("rpe_cdelprob"));
        residentialPerson.setGenderCode((String) resultRow.get("rpe_csex"));
        residentialPerson.setLanguageCode((String) resultRow.get("rpe_clanguage"));
        residentialPerson.setCodeOfAcademicTitle((String) resultRow.get("rpe_ctitle"));
        residentialPerson.setFirstName((String) resultRow.get("rpe_firstname"));
        residentialPerson.setLastName((String) resultRow.get("rpe_lastname"));
        residentialPerson.setSeparatorOfNames((String) resultRow.get("rpe_nameseparator"));
        residentialPerson.setMaidenName((String) resultRow.get("rpe_maidenname"));
        residentialPerson.setAdditionalAddressField((String) resultRow.get("rpe_addsuf"));
        residentialPerson.setStreetName((String) resultRow.get("rpe_street"));
        residentialPerson.setHouseNumber((String) resultRow.get("rpe_hnr"));
        residentialPerson.setZipCode((BigDecimal) resultRow.get("rpe_zip"));
        residentialPerson.setCityName((String) resultRow.get("rpe_city"));
        residentialPerson.setCantonAbbreviation((String) resultRow.get("rpe_canton"));
        residentialPerson.setBasicZip((BigDecimal) resultRow.get("rpe_gzip"));
        residentialPerson.setCoordinateReferenceOfBuilding((Long) resultRow.get("rpe_coo_id"));
        residentialPerson.setQuatityOfGeocoding((String) resultRow.get("rpe_ccoorq"));
        residentialPerson.setPoBoxNumber((String) resultRow.get("rpe_pob_nr"));
        residentialPerson.setPoBoxZip((BigDecimal) resultRow.get("rpe_pob_zip"));
        residentialPerson.setPoBoxCityName((String) resultRow.get("rpe_pob_city"));
        residentialPerson.setAddressValidDateStart((Date) resultRow.get("rpe_valid_from"));
        residentialPerson.setSourceOfBirthDate((String) resultRow.get("rpe_cbdatsrc"));
        residentialPerson.setBirthDate((Date) resultRow.get("rpe_birthdate"));
        residentialPerson.setBirthYear((BigDecimal) resultRow.get("rpe_birthyear"));
        residentialPerson.setOccupationalTitle((String) resultRow.get("rpe_occupation"));
        residentialPerson.setSourceOfEmail((String) resultRow.get("rpe_cemailsrc"));
        residentialPerson.setEmail((String) resultRow.get("rpe_email"));
        residentialPerson.setQualityOfPhoneDetails((String) resultRow.get("rpe_cphoneq"));
        residentialPerson.setPhoneAreaCode((String) resultRow.get("rpe_phoneareacode"));
        residentialPerson.setPhoneNumber((String) resultRow.get("rpe_phonenum"));
        residentialPerson.setQualityOfMobileDetails((String) resultRow.get("rpe_cmobileq"));
        residentialPerson.setMobileAreaCode((String) resultRow.get("rpe_mobileareacode"));
        residentialPerson.setMobileNumber((String) resultRow.get("rpe_mobilenum"));
        residentialPerson.setEntryDateOfDataRecord((Date) resultRow.get("rpe_reccreate_date"));
        residentialPerson.setModificationDateOfDataRecord((Date) resultRow.get("rpe_recmodify_date"));
        residentialPerson.setStatusOfPersonAddress((String) resultRow.get("rpe_cperadrstat"));
        
        return residentialPerson;
    }

}
