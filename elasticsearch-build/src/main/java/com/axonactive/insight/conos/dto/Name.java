package com.axonactive.insight.conos.dto;

/**
 * Created by ptanh2 on 4/29/2015.
 */
public class Name {
    private int mode;
    private String name;
    private String replacement;

    public Name() {

    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }
}
