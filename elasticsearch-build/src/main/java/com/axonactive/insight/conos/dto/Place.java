package com.axonactive.insight.conos.dto;

/**
 * Created by ptanh2 on 4/29/2015.
 */
public class Place {
    private int mode;
    private String placeName;
    private String replacementPlaceName;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getReplacementPlaceName() {
        return replacementPlaceName;
    }

    public void setReplacementPlaceName(String replacementPlaceName) {
        this.replacementPlaceName = replacementPlaceName;
    }

    public Place(int mode, String placeName, String replacementPlaceName) {

        this.mode = mode;
        this.placeName = placeName;
        this.replacementPlaceName = replacementPlaceName;
    }
}
