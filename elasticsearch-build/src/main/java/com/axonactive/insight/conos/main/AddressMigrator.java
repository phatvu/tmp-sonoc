package com.axonactive.insight.conos.main;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import javax.xml.bind.SchemaOutputResolver;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ptanh2 on 4/29/2015.
 */
public class AddressMigrator {
    private static  String NAME_PATH ;
    private static  String PLACE_PATH ;
    private static  String STREET_PATH;
    private static  String COMPANY_PATH;
    private static  String HNR_PATH;
    private Client client;
    private String index;
    private int zipCodeId = 0;
    public AddressMigrator(String host, int port, String clusterName, String index){
        Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", clusterName).build();

        TransportClient transportClient = new TransportClient(settings);
        transportClient = transportClient
                .addTransportAddress(new InetSocketTransportAddress(
                        host, port));
        client = (Client) transportClient;
        this.index = index;
    }

    protected void build(){
        getFile();
        migrateZipcode();
        migrateSynonymToES(HNR_PATH, "hnr");
        migrateSynonymToES(PLACE_PATH, "place");
        migrateSynonymToES(STREET_PATH, "street");
        migrateSynonymToES(COMPANY_PATH, "company");
        migrateSynonymToES(NAME_PATH, "name");
        System.out.println("Done");
    }

    private void getFile(){
        ClassLoader classLoader = getClass().getClassLoader();
        NAME_PATH = classLoader.getResource("Name.txt").getPath();
        PLACE_PATH = classLoader.getResource("Place.txt").getPath();
        STREET_PATH = classLoader.getResource("Street.txt").getPath();
        COMPANY_PATH = classLoader.getResource("Company.txt").getPath();
        HNR_PATH = classLoader.getResource("Hnr.txt").getPath();
    }


    private  void migrateSynonymToES(String filePath, String type){
        System.out.println("Start migrating \"" + type + "\"...");
        ObjectMapper objectMapper = new ObjectMapper();
        int id = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), StandardCharsets.ISO_8859_1));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] content = line.split("\\|");
                Map<String,String> map = new HashMap<>();
                map.put("mode", content[0]);
                map.put(type,content[1]);
                map.put("replacement",content.length == 2 ? "" : content[2]);
                client.prepareIndex(index, type).setId(String.valueOf(++id))
                        .setSource(objectMapper.writeValueAsString(map)).execute().actionGet();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finish migrating \"" + type + "\". Total record : " + id);
    }

    private void migrateZipcode(){
        System.out.println("Start migrating \"zipCode\"...");
        putZipcodeDataToES(1000, 1019);
        putZipcodeDataToES(1200, 1211);
        putZipcodeDataToES(1400, 1409);
        putZipcodeDataToES(1700, 1709);
        putZipcodeDataToES(1950, 1951);
        putZipcodeDataToES(2000, 2009);
        putZipcodeDataToES(2300, 2309);
        putZipcodeDataToES(2500, 2509);
        putZipcodeDataToES(3000, 3030);
        putZipcodeDataToES(3600, 3609);
        putZipcodeDataToES(4000, 4099);
        putZipcodeDataToES(4500, 4509);
        putZipcodeDataToES(4600, 4609);
        putZipcodeDataToES(4900, 4909);
        putZipcodeDataToES(5000, 5009);
        putZipcodeDataToES(5400, 5409);
        putZipcodeDataToES(6000, 6009);
        putZipcodeDataToES(6300, 6309);
        putZipcodeDataToES(6600, 6609);
        putZipcodeDataToES(6900, 6909);
        putZipcodeDataToES(7000, 7009);
        putZipcodeDataToES(8000, 8009);
        putZipcodeDataToES(8200, 8209);
        putZipcodeDataToES(8400, 8411);
        putZipcodeDataToES(8610, 8613);
        putZipcodeDataToES(8620, 8623);
        putZipcodeDataToES(8810, 8813);
        putZipcodeDataToES(9000, 9029);
        putZipcodeDataToES(9100, 9102);
        System.out.println("Finish migrating \"zipCode\". Total records : " + zipCodeId);
    }

    private void putZipcodeDataToES(int minCode, int maxCode){
        ObjectMapper objectMapper = new ObjectMapper();
        for ( int i = minCode; i <= maxCode; i++){
            Map<String,String> map = new HashMap<>();
            map.put("zip",String.valueOf(i));
            map.put("gzip",String.valueOf(minCode));
            try {
                client.prepareIndex(index,"zipcode").setId(String.valueOf(++zipCodeId)).setSource(objectMapper.writeValueAsString(map)).execute().actionGet();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }
}
