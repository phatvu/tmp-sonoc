package com.axonactive.insight.conos.dto;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by ttnhan on 4/29/2015.
 */
public class ResidentialPerson {
    private Long personAddressNumber;
    private Long personNumber;
    private Long householdNumber;
    private String statusOfPerson;
    private Date personInactivationDate;
    private Date lastConfirmationDate;
    private String statusOfAddress;
    private String sourceOfAddress;
    private String sourceOfRelocation;
    private String deliveryProbability;
    private String genderCode;
    private String languageCode;
    private String codeOfAcademicTitle;
    private String firstName;
    private String firstNameSYN;
    private String lastName;
    private String separatorOfNames;
    private String maidenName;
    private String additionalAddressField;
    private String streetName;
    private String houseNumber;
    private BigDecimal zipCode;
    private String cityName;
    private String cantonAbbreviation;
    private BigDecimal basicZip;
    private Long coordinateReferenceOfBuilding;
    private String quatityOfGeocoding;
    private String poBoxNumber;
    private BigDecimal poBoxZip;
    private String poBoxCityName;
    private Date addressValidDateStart;
    private String sourceOfBirthDate;
    private Date birthDate;
    private BigDecimal birthYear;
    private String occupationalTitle;
    private String sourceOfEmail;
    private String email;
    private String qualityOfPhoneDetails;
    private String phoneAreaCode;
    private String phoneNumber;
    private String qualityOfMobileDetails;
    private String mobileAreaCode;
    private String mobileNumber;
    private Date EntryDateOfDataRecord;
    private Date modificationDateOfDataRecord;
    private String statusOfPersonAddress;

    public String getFirstNameSYN() {
        return firstNameSYN;
    }

    public void setFirstNameSYN(String firstNameSYN) {
        this.firstNameSYN = firstNameSYN;
    }

    public Long getPersonAddressNumber() {
        return personAddressNumber;
    }

    public void setPersonAddressNumber(Long personAddressNumber) {
        this.personAddressNumber = personAddressNumber;
    }

    public Long getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(Long personNumber) {
        this.personNumber = personNumber;
    }

    public Long getHouseholdNumber() {
        return householdNumber;
    }

    public void setHouseholdNumber(Long householdNumber) {
        this.householdNumber = householdNumber;
    }

    public String getStatusOfPerson() {
        return statusOfPerson;
    }

    public void setStatusOfPerson(String statusOfPerson) {
        this.statusOfPerson = statusOfPerson;
    }

    public Date getPersonInactivationDate() {
        return personInactivationDate;
    }

    public void setPersonInactivationDate(Date personInactivationDate) {
        this.personInactivationDate = personInactivationDate;
    }

    public Date getLastConfirmationDate() {
        return lastConfirmationDate;
    }

    public void setLastConfirmationDate(Date lastConfirmationDate) {
        this.lastConfirmationDate = lastConfirmationDate;
    }

    public String getStatusOfAddress() {
        return statusOfAddress;
    }

    public void setStatusOfAddress(String statusOfAddress) {
        this.statusOfAddress = statusOfAddress;
    }

    public String getSourceOfAddress() {
        return sourceOfAddress;
    }

    public void setSourceOfAddress(String sourceOfAddress) {
        this.sourceOfAddress = sourceOfAddress;
    }

    public String getSourceOfRelocation() {
        return sourceOfRelocation;
    }

    public void setSourceOfRelocation(String sourceOfRelocation) {
        this.sourceOfRelocation = sourceOfRelocation;
    }

    public String getDeliveryProbability() {
        return deliveryProbability;
    }

    public void setDeliveryProbability(String deliveryProbability) {
        this.deliveryProbability = deliveryProbability;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getCodeOfAcademicTitle() {
        return codeOfAcademicTitle;
    }

    public void setCodeOfAcademicTitle(String codeOfAcademicTitle) {
        this.codeOfAcademicTitle = codeOfAcademicTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSeparatorOfNames() {
        return separatorOfNames;
    }

    public void setSeparatorOfNames(String separatorOfNames) {
        this.separatorOfNames = separatorOfNames;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public String getAdditionalAddressField() {
        return additionalAddressField;
    }

    public void setAdditionalAddressField(String additionalAddressField) {
        this.additionalAddressField = additionalAddressField;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public BigDecimal getZipCode() {
        return zipCode;
    }

    public void setZipCode(BigDecimal zipCode) {
        this.zipCode = zipCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCantonAbbreviation() {
        return cantonAbbreviation;
    }

    public void setCantonAbbreviation(String cantonAbbreviation) {
        this.cantonAbbreviation = cantonAbbreviation;
    }

    public BigDecimal getBasicZip() {
        return basicZip;
    }

    public void setBasicZip(BigDecimal basicZip) {
        this.basicZip = basicZip;
    }

    public Long getCoordinateReferenceOfBuilding() {
        return coordinateReferenceOfBuilding;
    }

    public void setCoordinateReferenceOfBuilding(Long coordinateReferenceOfBuilding) {
        this.coordinateReferenceOfBuilding = coordinateReferenceOfBuilding;
    }

    public String getQuatityOfGeocoding() {
        return quatityOfGeocoding;
    }

    public void setQuatityOfGeocoding(String quatityOfGeocoding) {
        this.quatityOfGeocoding = quatityOfGeocoding;
    }

    public String getPoBoxNumber() {
        return poBoxNumber;
    }

    public void setPoBoxNumber(String poBoxNumber) {
        this.poBoxNumber = poBoxNumber;
    }

    public BigDecimal getPoBoxZip() {
        return poBoxZip;
    }

    public void setPoBoxZip(BigDecimal poBoxZip) {
        this.poBoxZip = poBoxZip;
    }

    public String getPoBoxCityName() {
        return poBoxCityName;
    }

    public void setPoBoxCityName(String poBoxCityName) {
        this.poBoxCityName = poBoxCityName;
    }

    public Date getAddressValidDateStart() {
        return addressValidDateStart;
    }

    public void setAddressValidDateStart(Date addressValidDateStart) {
        this.addressValidDateStart = addressValidDateStart;
    }

    public String getSourceOfBirthDate() {
        return sourceOfBirthDate;
    }

    public void setSourceOfBirthDate(String sourceOfBirthDate) {
        this.sourceOfBirthDate = sourceOfBirthDate;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(BigDecimal birthYear) {
        this.birthYear = birthYear;
    }

    public String getOccupationalTitle() {
        return occupationalTitle;
    }

    public void setOccupationalTitle(String occupationalTitle) {
        this.occupationalTitle = occupationalTitle;
    }

    public String getSourceOfEmail() {
        return sourceOfEmail;
    }

    public void setSourceOfEmail(String sourceOfEmail) {
        this.sourceOfEmail = sourceOfEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQualityOfPhoneDetails() {
        return qualityOfPhoneDetails;
    }

    public void setQualityOfPhoneDetails(String qualityOfPhoneDetails) {
        this.qualityOfPhoneDetails = qualityOfPhoneDetails;
    }

    public String getPhoneAreaCode() {
        return phoneAreaCode;
    }

    public void setPhoneAreaCode(String phoneAreaCode) {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getQualityOfMobileDetails() {
        return qualityOfMobileDetails;
    }

    public void setQualityOfMobileDetails(String qualityOfMobileDetails) {
        this.qualityOfMobileDetails = qualityOfMobileDetails;
    }

    public String getMobileAreaCode() {
        return mobileAreaCode;
    }

    public void setMobileAreaCode(String mobileAreaCode) {
        this.mobileAreaCode = mobileAreaCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Date getEntryDateOfDataRecord() {
        return EntryDateOfDataRecord;
    }

    public void setEntryDateOfDataRecord(Date entryDateOfDataRecord) {
        EntryDateOfDataRecord = entryDateOfDataRecord;
    }

    public Date getModificationDateOfDataRecord() {
        return modificationDateOfDataRecord;
    }

    public void setModificationDateOfDataRecord(Date modificationDateOfDataRecord) {
        this.modificationDateOfDataRecord = modificationDateOfDataRecord;
    }

    public String getStatusOfPersonAddress() {
        return statusOfPersonAddress;
    }

    public void setStatusOfPersonAddress(String statusOfPeronaddress) {
        this.statusOfPersonAddress = statusOfPeronaddress;
    }
}
