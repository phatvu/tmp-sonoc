package com.axonactive.insight.conos.dto;

/**
 * Created by ptanh2 on 4/29/2015.
 */
public class Company {
    private int mode;
    private String companyName;
    private String replacementCompanyName;

    public Company(int mode, String companyName, String replacementCompanyName) {
        this.mode = mode;
        this.companyName = companyName;
        this.replacementCompanyName = replacementCompanyName;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getReplacementCompanyName() {
        return replacementCompanyName;
    }

    public void setReplacementCompanyName(String replacementCompanyName) {
        this.replacementCompanyName = replacementCompanyName;
    }
}
