package com.axonactive.insight.conos.dto;

/**
 * Created by ptanh2 on 4/29/2015.
 */
public class Street {
    private int mode;
    private String streetName;
    private String replacementStreetName;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getReplacementStreetName() {
        return replacementStreetName;
    }

    public void setReplacementStreetName(String replacementStreetName) {
        this.replacementStreetName = replacementStreetName;
    }

    public Street(int mode, String streetName, String replacementStreetName) {
        this.mode = mode;
        this.streetName = streetName;
        this.replacementStreetName = replacementStreetName;
    }
}
