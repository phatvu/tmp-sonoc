package com.axonactive.insight.conos.main;

/**
 * Created by ttnhan on 5/4/2015.
 */
public class Main {
    public static void main(String[] args) {

        final String host = args[0];
        final Integer port = Integer.parseInt(args[1]);
        final String clusterName = args[2];
        final String indexName = args[3];

        if (indexName.contains("rpe") || indexName.equals("con")) {
            final boolean isBuildAll = args[4].equals("all");
            final Long bulkSize = Long.parseLong(args[5]);
            final String typeName = args[6];
            final String idType = args[7];

            ResidentialPersonESBuilder rpm = new ResidentialPersonESBuilder(
                    host,
                    port,
                    clusterName,
                    indexName,
                    typeName
            );

            rpm.build(isBuildAll, bulkSize, idType.equals("original-id"));

        } else if (indexName.equals("nameExceptionES")) {
            AddressMigrator addressMigrator = new AddressMigrator(host, port, clusterName, indexName);
            addressMigrator.build();
        }
    }
}
