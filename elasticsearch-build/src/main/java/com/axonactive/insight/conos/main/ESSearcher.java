package com.axonactive.insight.conos.main;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ttnhan on 5/5/2015.
 */

public class ESSearcher {
    private static ESSearcher instance;
    private Client client;
    private SearchRequestBuilder request;

    public static final String ELASTIC_SEARCH_HOST = "192.168.70.99";
    public static final Integer ELASTIC_SEARCH_API_PORT = 9300;
    public static final String ELASTIC_SEARCH_CLUSTER_NAME = "conos";

    private ESSearcher() {
        Settings settings = ImmutableSettings.settingsBuilder().put("cluster.name", ELASTIC_SEARCH_CLUSTER_NAME).build();
        this.client = new TransportClient(settings).addTransportAddress(new InetSocketTransportAddress(ELASTIC_SEARCH_HOST, ELASTIC_SEARCH_API_PORT));
        this.request = client.prepareSearch();
        this.request.setSearchType(SearchType.DFS_QUERY_THEN_FETCH);
    }

    public static ESSearcher getInstance() {
        if (instance == null) {
            instance = new ESSearcher();
        }
        return instance;
    }

    public void searchOnIndex(String... indices) {
        this.request.setIndices(indices);
    }
    public void searchOnType(String... types) {
        this.request.setTypes(types);
    }

    public void searchString(String fieldName, String value) {
        this.request.setQuery(QueryBuilders.termQuery(fieldName, value.toLowerCase()));
    }

    public void searchIntegerRange(String fieldName, int fromValue, int toValue) {
        this.request.setPostFilter(FilterBuilders.rangeFilter(fieldName).from(fromValue).to(toValue));
    }

    public Object getUniqueResults(Class clazz) throws NonUniqueResultException {
        List<Object> listResults = getResults(clazz);
        if (listResults.isEmpty()) {
            return null;
        }
        if (listResults.size() > 1){
            throw new NonUniqueResultException(listResults);
        }
        return listResults.get(0);
    }

    public List<Object> getResults(Class clazz) {
        return getResults(clazz, -1, -1);
    }

    public List<Object> getResults(Class clazz, int offset, int limit) {
        if (offset > 0) {
            this.request.setFrom(offset);
        }
        if (limit > 0) {
            this.request.setSize(limit);
        }

        SearchResponse searchResponse = this.request.execute().actionGet();
        ObjectMapper mapper = new ObjectMapper();
        List<Object> result = new ArrayList<>();

        long searchTotalHits = searchResponse.getHits().getTotalHits();
        long returnSize;
        if (limit > 0) {
            returnSize = (searchTotalHits < limit)? searchTotalHits: limit;
        } else {
            returnSize = searchTotalHits;
        }
        for (int i = 0; i < returnSize; i++) {
            try {
                byte[] json = searchResponse.getHits().getAt(i).getSourceAsString().getBytes("UTF-8");
                Object obj = mapper.readValue(json, clazz);
                result.add(obj);
            } catch (Exception e) {
                e.printStackTrace();
                break;
            }
        }

        return result;
    }

    public void disconnect() {
        this.client.close();
    }
}

class NonUniqueResultException extends Exception {
    private List resultSet;

    public NonUniqueResultException(List resultSet) {
        super("Non-unique result found in get unique result call");
        this.resultSet = resultSet;
    }

    public List getResultSet() {
        return resultSet;
    }
}
